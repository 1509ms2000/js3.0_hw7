const container = document.querySelector(".container-users");
const blockContainer = document.querySelector(".container");
const followersRepos = document.querySelector('.followers_repos');
const userInformation = document.querySelector('.user-information');
const input = document.querySelector('.input')
let maxQuantityUsers = 5;
let currentQuery;
let userId;


function trackChange(value) {
    if (currentQuery) {
        getUrl(value);
    } else {
        getUrl(value);
        setTimeout(() => {
            if (currentQuery) {
                clearContainer(container)
                sendRequest(currentQuery);
            } else {
                clearContainer(container);
            }
        }, 1500)
    }


}

function getUrl(value) {
    currentQuery = value ? `https://api.github.com/search/users?q=${value}&per_page=${maxQuantityUsers}` : '';

}

function clearContainer(container) {
    while (container.hasChildNodes()) {
        container.removeChild(container.firstChild);
    }
}

function sendRequest(query) {
    fetch(query)
        .then(e => {
            if (e.ok) {
                return e.json()
            }else {
                throw new Error('request error ')
            }
        })
        .then(res => {
            clearContainer(container);
            clearContainer(userInformation);
            clearContainer(followersRepos);
            setContainer(res);

        })
        .catch(e=>{
            console.error(e)
        })
}


function setContainer(res) {
    res.items.forEach((user) => {
        const userDiv = `<div class="user" id="${user.login}" onclick="showCurrentUser.call(this)" class="user">
    <img src="${user.avatar_url}" alt="avatar_url"/>
    <p>${user.login}</p>
    </div>`
        container.insertAdjacentHTML("beforeend", userDiv);
        // container.append(containerUser)
        currentQuery = '';
    })
}

function showCurrentUser() {
    userId = this.id
    clearContainer(container);
    clearContainer(userInformation);
    clearContainer(followersRepos);
    input.value = '';
    blockContainer.style.justifyContent = 'space-around'
    showFollowers();
    showRepos();
    getUserInfo();
}

function showFollowers() {
    // const userName = this.parentElement.id
    fetch(`https://api.github.com/users/${userId}/followers?per_page=${maxQuantityUsers}`)
        .then(e => {
            if (e.ok) {
                return e.json()
            }else {
                throw new Error('request error ')
            }
        })
        .then(result => {
            let followers = '<div  class="user-followers"><p>Followers</p></div>';
            const wrapper = document.createElement('div')
            result.forEach(follower => {
                followers += `<div  class="user-followers">
                                <img src="${follower.avatar_url}" alt="avatar_url"/>
                                    <p><a target="_blank" href="${follower.html_url}">${follower.login}</a></p>
                                </div>`

            })
            wrapper.insertAdjacentHTML("beforeend", followers);
            followersRepos.append(wrapper)
        })
        .catch(e=>console.error(e))
}

function showRepos() {
    // const userRepos = this.parentElement.id
    fetch(`https://api.github.com/users/${userId}/repos?per_page=${maxQuantityUsers}`)
        .then(e => {
            if (e.ok) {
                return e.json()
            }else {
                throw new Error('request error ')
            }
        })
        .then(result => {
            let reposUser = '<div  class="user-repos"><p>Repositories</p></div>'
            const wrapper = document.createElement('div')
            result.forEach(repos => {
                reposUser += `<div  class="user-repos">
                                    <p>${repos.name}</p>
                                     <p><a target="_blank" href="${repos.html_url}" >Show repos</a></p>                                     
                                </div>`
            })
            wrapper.insertAdjacentHTML("beforeend", reposUser);
            followersRepos.append(wrapper)
        })
        .catch(e=>console.error(e))
}

function getUserInfo() {
    fetch(`https://api.github.com/users/${userId}`)
        .then(e => {
            if (e.ok) {
                return e.json()
            }else {
                throw new Error('request error ')
            }
        })
        .then(user => {
            const userDiv = `<div class="user"  id="${user.login}" >
                <img src="${user.avatar_url}" alt="avatar_url"/>
                <p><a target="_blank" href="${user.html_url}">${user.login}</a></p>
                </div>`
            userInformation.insertAdjacentHTML("beforeend", userDiv);
        })
        .catch(e=>console.error(e))

}

function getAllUsers() {
    maxQuantityUsers = 100;
    if (input.value) {
        clearContainer(container);
        clearContainer(userInformation);
        clearContainer(followersRepos);
        trackChange(input.value);
        // console.log("я тут");
    }
    maxQuantityUsers = 5;
}
